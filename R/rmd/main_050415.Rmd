---
title: "Using kinetic model fit RNAseq profile (1)"
author: "Frank"
date: "`r date()`"
output: 
    html_document:
      toc: true
      number_sections: true
      highlight: haddock
---
# data
## look at ev data
Plot ev profiles 
```{r,echo=FALSE}
load('./data/ev_tnf.RData')
load('./data/wt_tnf.RData')
load('./data/dbd_tnf.RData')
load('./data/final_list.RData')

times <- seq(0,8,by=0.01)
times<- c(0,.5,1,3,8)
tcPlot <- function(dataname){
  data<-get(dataname)
  data <- data[final_list,]
  data <- data+1
  data[,1:5] <- sapply(1:5,function(x) data[,x]/data[,1])
  data[,6:10] <- sapply(6:10,function(x) data[,x]/data[,6])
  matplot(times,t(data[,6:10]),type="b",col=1,lty=2,pch=16,main=dataname,ylab="fold")
  matpoints(times,t(data[,1:5]),type="b",col=1,lty=1,pch=1)
  }
par(mfrow=c(1,2))
tcPlot("ev_tnf")
tcPlot("wt_tnf")

```
We can see from the plot that the majority of genes have a flat patten, except two of them show obviously induction at 1hr and 3hr time points. Not much information I can get from this plot. 

So the strategies of fitting these ev dataset, is 
1. Use a flat input first 
2. Then change to a linear increased input 

## hierachical clustering 
```{r}
library(gplots)
library(RColorBrewer)
load('./data/ev_tnf.RData')
load('./data/wt_tnf.RData')
load('./data/dbd_tnf.RData')
load('./data/final_list.RData')
load("./data/meanData.RData")
ev_tnf <- ev_tnf[final_list,]
wt_tnf <- wt_tnf[final_list,]
dbd_tnf <- dbd_tnf[final_list,]


#?dist
#d <- dist(ev_tnf)
#hc <- hclust(d)
#?hclust
#plot(hc,ann=F,axes=F)
my_palette <- colorRampPalette(c("green", "yellow", "red"))(n = 299)

heatmap.2(ev_tnf,
          Colv =NA,
          dendrogram ="row",
          trace="none",
          col=my_palette,
          margins =c(12,9))

rownames(ev_tnf)<-GeneNames[rownames(ev_tnf)]
rownames(wt_tnf)<-GeneNames[rownames(wt_tnf)]
save(file='./data/ev_tnf2.RData','ev_tnf')
save(file='./data/wt_tnf2','wt_tnf')
```


# Model
## Model (1): Basic model
$$\frac{dmRNA_i}{dt} =\alpha_{i0} + \alpha_{i}\cdot f(Rela(t)) - \beta_i\cdot mRNA$$

* 1st-order mass action (Simplest, no additional parameters) 
* Or MM kinetics: $f(RelA) = \frac{RelA(t)}{RelA(t)+K_{Di}}$

The initial state is calculated by assume the transcription is at steady state:   
$$mRNA_i(0) = \frac{\alpha_{i0}+\alpha_i\cdot f(Rela(t=0))}{\beta_i}$$

## Model (2): the $\Delta$ Model
<font color="blue">**With TFx (unknown transcriptional factors)**</font>

For gene i in **Rela WT** condition:
$$\frac{dmRNA^{wt}_i}{dt} =\alpha^{wt}_{i0} + \alpha^{wt}_{i}\cdot f^{wt}(Rela(t)) + \alpha^{wt}_{ix}\cdot g^{wt}(TFx(t)) - \beta^{wt}_i\cdot mRNA^{wt}_i$$

For the same gene but in **EV** condition, basic RelA activity is assumed to reduced to $0$, so equation: 
$$\frac{dmRNA^{ev}_i}{dt} =\alpha^{ev}_{i0} + \alpha^{ev}_{ix}\cdot g^{ev}(TFx(t)) - \beta^{ev}_i\cdot mRNA^{ev}_i$$

<font color="blue">**The assumptions:**</font>

1. The function of TFx and RelA can be seperatiable. 
2. TFx activity and capability does not change between EV and Rela wt: $\alpha^{wt}_{ix}\cdot g^{wt}(TFx(t)) =\alpha^{ev}_{ix}\cdot g^{ev}(TFx(t))$
3. mRNA half-life dosent change between mutants. $\beta^{ev}_i = \beta^{wt}_i$

Based on these assumptions, we can get the $\Delta$ Model: 
$$\frac{d\Delta mRNA^{wt-ev}_i}{dt}  = \Delta\alpha^{wt-ev}_{i0}+ \alpha^{wt}_{i}\cdot f^{wt}(Rela(t))  - \beta_i\cdot \Delta mRNA^{wt-ev}_i$$

The advantages of using $\Delta$ model is reduced the times for Fitting from 2 (wt and EV separatively) to 1 (now only fit the $\Delta$ model). In this way, we also eliminate the unknown effects of the TFx. 

<font color="blue">**the $\Delta$ Model generalization**</font>

For compare any two conditions: ev and muntant, it is :

$$\frac{d\Delta mRNA^{mutant-ev}_i}{dt} =  \Delta\alpha^{mutant-ev}_{i0}+ \alpha^{mutant}_{i}\cdot f^{mutant}(Rela^{mutant}(t)) - \beta_i\cdot\Delta mRNA^{mutant-ev}_i$$

# The project logic 

Use $\Delta$ model to fit RNAseq data for each genes (pairwisely comparing any mutant or wt (m) with EV):

1. How good the model explains the data? 
2. What are the 95% confidential ranges for: $\Delta\alpha^{m-ev}_{i0}$ basal transcription rate constant and induced transcriptional rate constant $\alpha^m_i$ for gene $i$ in genotype $m$.
3. Then we can cluster genes based on vectors: $[\Delta\alpha^{m-ev}_{i0},\alpha^m_i,\beta_i], i = 1 ... num(genes).$ 
4. Do 1-3 again for other genotypes $n$ and etc. *later, to be developed*

# Fitting targets 

<font color="blue">**initial $\Delta mRNA$ concentration**</font>

Make assumptions that at time 0, 1) mRNA production is at steady state and 2) $f^{wt/mutant}(Rela=0)=0$. Then 
$$ \Delta mRNA_i^{wt-ev}(t=0) = \frac{\Delta\alpha_{i0}^{wt-ev}}{\beta_i}$$
So $\Delta\alpha_{i0}^{wt-ev} = \beta_i \cdot \Delta mRNA_i^{wt-ev}(t=0)$.

If only has assumption 1), then 
$$\Delta\alpha_{i0}^{wt-ev} = \beta_i \cdot \Delta mRNA_i^{wt-ev}(t=0)-\alpha_i^{wt}\cdot f^{wt}(Rela(t=0))$$. So $\Delta mRNA_i^{wt-ev}(t=0)$ is a function of both basal and induced transcrition rates. 

We can use basal mRNA reads (for ev and wt, we have tech replicates) and the known degradation rate $\beta_i$ to calculate basal rate of transcription $\alpha_{i0}^{wt-ev}$ directly, if both 1) and 2) are satisfied. 

<font color="blue">**The target parameters**</font>

  Name |  Symbol | source
------------- | ------------- | -------------
basal tr rate | $\alpha_{i0}^{mutant-ev}$ |  Fit
induced tr rate | $\alpha_{i}^{mutant-ev}$| Fit
deg rate | $\beta_i$ | References

## fitting notes (1): Current metrics/objective functions 
We used similar 'fraction of explained variance' as objective function [Rabani et al. 2011](http://www.nature.com/nbt/journal/v29/n5/extref/nbt.1861-S1.pdf) to estiminate the goodness of fit of the model. 
$$fractionOfExplainedVariance = \frac{SS_{model}}{SS_{Total}} = \frac{SS_{Total}-SS_{resids}-SS_{unknown}}{SS_{Total}}$$

For linear regression model (linear model), the $SS_{unknown}=0$. 

The rsquare function I currently use is defined as follow: 
$$r^2 = 1- \frac{var(resids)}{var(data)} $$

So $fractionOfExplainedVariance\leq\frac{SS_{Total}-SS_{resids}}{SS_{Total}} = r^2$. I.e.$r^2$ is the upper boundary (when linear regression, it is equal) of 'fraction of explained variance'. 

## fitting notes (2):  Link $r^2$ to likelihood function (probability)
The likelihood is defined as $L(\theta|X) = p(x|\theta)$
In the Beyesian formalism: 
$$Posterior = p(M|D) = \frac{p(D|M)\cdot p(M)}{p(D)} = \frac{Likelihood \cdot p(M)}{p(D)}$$,
in which $p(M)$ is the **prior** and $p(D)$ is often assumed equal to all the models. So $Posterior \approx Likelihood \cdot p(M)$

+ In [Jovanovic et al. Science 2015](http://www.sciencemag.org/content/347/6226/1259038.short), they assumed a Norm distribution for the protein fraction at each time point, with mean equal to the mean of the experiment data and sd equals to the estimated variance of the corresponding data. So likelihood is equal to `dnorm(xsim,mean =xdata, sd = sd_xdata)`. Also they used `-log(likelihood)` to transfer multiple data points. They minimized the `-log(likelihood)` function and get the best estimated parameters and using Hessian to access statisitcs associated with the best fit and use the similar concept of $r^2$ to access the best fit quality (at least in their [cell 2014](http://www.ncbi.nlm.nih.gov/pubmed/25497548) paper). 
    + From Hessian to confidential interval and related statistics can also found in `../06_getstats.Rmd` and [here](http://stats.stackexchange.com/questions/27033/in-r-given-an-output-from-optim-with-a-hessian-matrix-how-to-calculate-paramet). 
    + They are using emperical Bayesian method, last time's fitting results are used as prior for current fitting iterate. 
    + They are tried to do maximum-posterior estimate (MAP) 

## fitting notes (2):  Link $r^2$ to likelihood function (probability)
+ In the physical society, people use some transformation from RSS to get the likelihood function (see [Brown & Sethna et al. PRE 2003](http://www.ncbi.nlm.nih.gov/pubmed/14525003) or [Koger group's paper]((http://onlinelibrary.wiley.com/doi/10.1038/msb.2012.69/full)). $$p(D|M) \sim exp(-C(\theta)/T)$$, in which $C(\theta)$ is the cost function, defined by $C(\theta) = \sum\limits_t\sum\limits_i\frac{x^i_{model}(t;\Theta)-x^i_{data}(t)}{2\sigma^2_{data}(t)}$, with an assumption that the resids are normally distributed (?). Then by applying MCMC method with some sort Metropolis-Hastings criterion to get a stable MCMC chain and use this chain to access the posterior distribution and related statisitics. So this method is usually termed as **ensemble method**. 
    + Ensemble approach generates better measure of model softness than H (Hessian matrix)
    + Ensemble method samples the full nonlinear cost space 
    + H is a quadratic approximation to the actual shape 
    + L (Levenberg-Marquardt Hessian) is an approximation to H 

## Take acount of small reads. 

~~We have technique replicates. When have biological replicates, the reads can be assumed to n[egative binomial distribution](http://en.wikipedia.org/wiki/Negative_binomial_distribution).~~ I don't think we need to go to so deep into the statisics. We can use the information of the variance of the data and put that into the objective function (or [Log-likelihood](http://www.sciencedirect.com/science/article/pii/S0092867414014469)).

# Model input 
## Model input: mRNA halflife

+ ~~Our microarray data set `0, 3, 8h` after ActD~~
+ Christine et al. `createChrinstine.R`: 7 genes
+ Hao et al. 2009 `createHao.R` and `hao.RData`: 
    + The half life is measured at: basal, TNF 0.5h and TNF6 h 
    + In future: need to interplot the halflife 
    + dimension: `r load('./data/hao.RData');dim(hao)`
+ `online.RData`: dimension is `r load('./data/online.RData');dim(online)`, ref is (http://lgsun.grc.nia.nih.gov/mRNA/)
    + unique gene number: `r length(unique(online$gene))`
    + Took average of halflife for the non-unique ones 


## Model input: RelA activity 
<font color="blue">**Experimental quantification**</font>

RelA activity in rela wt is quantified as follow: 

```{r,echo=FALSE,fig.align='center',fig.width=3,fig.height=3}
load(file="./data/nfkb.RData")
plot(nfkb.rela,type='b',ylab='Rela emsa',xlab='Time (h)',xlim=c(0,8),ylim=c(0,1.5))
```

Note: In order to get the fit, I manually added one data point at 8hr. 

## RNAseq 
Genotype  | Time points| Replicates | Simuli
------------- | ------------- | ------------- | -------------
ev p53-/-rela-/- | 0,0.5,1,3,8 h | Y | Lps & TNF
rela wt  | 0,0.5,1,3,8 h | Y | Lps & TNF
DBD mutant  | 0,0.5,1,3,8 h | N | Lps & TNF

#  Result
## Compare wt and delta wt and ev fit:
<font color="blue">**Percentage of good fit ($R^2>=0.5$):**<font>

<font color="red">`r load("./data/results.RData");round(sum(rsq>.5,na.rm = T)/length(rsq)*100*100)/100`%.</font> in wt vs. <font color="red">`r load("./data/deltaWTEVresults.RData");round(sum(rsq>.5,na.rm = T)/length(rsq)*100*100)/100`%.</font> in $\Delta$ Model

```{r,echo=FALSE,fig.align="center",fig.width=6,fig.height=3}
load("./data/wt_tnfresults.RData");par(mfrow=c(1,2))
plot(rsq[ord_idx],type="b",ylab="R^2",main = "wt Model",ylim=c(0,1))
abline(0.5,0,col=grey(level = .5))
load("./data/deltaWTEVresults.RData")
plot(rsq[ord_idx],type="b",ylab="R^2",main = "Delta Model",ylim=c(0,1))
abline(0.5,0,col=grey(level = .5))
```

## Plot all the Fit (`r load("./data/deltaWTEVresults.RData");length(ord_idx)`):

```{r,echo=FALSE,fig.width=9,fig.height=12}
par(mfrow = c(11,7),mar=c(2,2,2,2)+0.1)
for(x in 1:length(ord_idx)) plotFit(results[[ord_idx[x]]])
```
