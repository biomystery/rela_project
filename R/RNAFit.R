#rm(list=ls(all=T))                     # Correct directory and clear 
library(deSolve)                                        # model ODE


## function runmodel: get ode solution 
runModel<- function (nfkb_input,pars,times){
  k0 <- pars$k0;k1<-pars$k1;k2<-pars$k2;
  mRNAini <- c(mRNA= (k0+k1*nfkb_input$val[1])/k2) # init,ss assumption

  ## subroutine: The model
  odeModel <- function (Time, State, Pars) {
    with(as.list(c(State, Pars)),{
      nfkb <-approxfun(nfkb_input$time,nfkb_input$val)(Time)
      dmRNA    <- (k0+k1*nfkb)-k2*mRNA
      return(list(c(dmRNA)))
    })
  }
  out   <- ode(mRNAini, times, odeModel, pars)
  out<-as.data.frame(out)
}

# optimization for EV 
runOptimEV <- function(initP,lb,ub,gene,con,deg_flag=F){ # con is the rnaseq count file name

    objEV <- function(pars){
    times <- seq(0,8,by=0.01);
    if(deg_flag){
        out<-runModel(nfkb_input = data.frame(time=c(0,.5,1,3,8), val = inputGenFuc(gene)),
                    data.frame(k0=pars[1],k1=pars[2],
                               k2=pars[3]),times)
    }
    else{
      out<-runModel(nfkb_input = data.frame(time=c(0,.5,1,3,8), val = inputGenFuc(gene)),
                    data.frame(k0=pars[1],k1=pars[2],
                               k2=data.kdeg[[gene]]),times)
    }
    out<-as.data.frame(out)
    out$mRNA[length(out$time)] <- out$mRNA[length(out$time)-1]
    idx<- which(out$time %in% mRNAprofile$time)
    rmsd <- sqrt(sum((out$mRNA[idx] - mRNAprofile$val)^2)/length(mRNAprofile$time))
  }
  
  mRNAprofile<-data.frame(time=c(0,.5,1,3,8),val = con[gene,1:5])
  head(mRNAprofile)
  
  fit <- try(nlminb(start = initP,objEV,lower=lb,upper =ub))
  
  times <- seq(0,8,by=0.01)
  out<-runModel(nfkb_input = data.frame(time=c(0,.5,1,3,8), val = inputGenFuc(gene)),
                data.frame(k0=fit$par[1],k1=fit$par[2],
                           k2=data.kdeg[[gene]]),times)
  out <- as.data.frame(out)
  out$mRNA[length(out$time)] <- out$mRNA[length(out$time)-1]
  fit$expdata<-mRNAprofile
  fit$bestFit <- out
  fit$gene <- gene
  fit$resid <- out$mRNA[(out$time %in% mRNAprofile$time)] - mRNAprofile$val 
  fit$rsquared <- 1-var(fit$resid)/var(mRNAprofile$val)
  return(fit)
}


## run optim 
runOptim <- function(initP,lb,ub,gene,con){ # con is the rnaseq count file name
  ## obj function 
  obj <- function(pars){
    
    times <- seq(0,8,by=0.01);
    out<-runModel(nfkb_input = nfkb.rela,data.frame(k0=pars[1],k1=pars[2],
                                                    k2=lookupHalflife(tolower(gene))),times)
    out<-as.data.frame(out)
    idx<- which(out$time %in% mRNAprofile$time )
    rmsd <- sqrt(sum((out$mRNA[idx] - mRNAprofile$val)^2)/length(mRNAprofile$time))
  }
  
  idx <- which(tolower(GeneNames)==tolower(gene))
  mRNAprofile<-data.frame(time=c(0,.5,1,3,8),val = con[idx,1:5])
  head(mRNAprofile)
  
  fit <- try(nlminb(start = initP,obj,lower=lb,upper =ub))
  times <- seq(0,8,by=0.01)
  out<-runModel(nfkb_input = nfkb.rela,data.frame(k0=fit$par[1],k1=fit$par[2],
                                                  k2=lookupHalflife(tolower(gene))),times)
  fit$expdata<-mRNAprofile
  fit$bestFit <- as.data.frame(out)
  fit$gene <- gene
  fit$resid <- out$mRNA[(out$time %in% mRNAprofile$time)] - mRNAprofile$val 
  fit$rsquared <- 1-var(fit$resid)/var(mRNAprofile$val)
  return(fit)
}

# plot fit
plotFit <- function(fit){
  ylims=c(0,max(c(fit$bestFit$mRNA,fit$expdata$val),na.rm = T));
  if(is.infinite(ylims[2])) ylims[2]<-max(fit$expdata$val)
  plot(fit$bestFit,type='l',ylim=ylims,
       main=paste(fit$gene,' R^2=',round(fit$rsquared*100)/100))
  points(fit$expdata)
}

plotFit2 <- function(fit){
  points(fit$bestFit,type='l',ylim=c(0,max(c(fit$bestFit$mRNA,fit$expdata$val))),
       main=paste(fit$gene,' R^2=',round(fit$rsquared*100)/100),col=gray(50))
}

## test run 
testRunModel <- function(){
  load("./data/chrinstine.RData")
  load("./data//wt_tnf.RData")
  load("./data//nfkb.RData")
  times <- seq(0,8,by=0.01)
  
  library(deSolve)
  out<-runModel(nfkb_input = nfkb.rela,data.frame(k0=0,k1=1,k2=.2),times)
  plot(out[,1],out[,2],type='l',ylim=c(0,1))
  out<-runModel(nfkb_input = nfkb.rela,data.frame(k0=0,k1=1,k2=.8),times)
  lines(out[,1],out[,2],type='l',col=2)
  out<-runModel(nfkb_input = nfkb.rela,data.frame(k0=0,k1=100,k2=chrinstine$kdeg[tolower(chrinstine$gene)=='tnf'],
                                                  k3=0.5),times)
  lines(out[,1],out[,2],type='l',col=3)
  
  # test obj
  gene <- 'traf1'
  gene<-'Tnfaip3'
  idx <- which(tolower(GeneNames)==gene)
  mRNAprofile<-data.frame(time=c(0,.5,1,3,8),val = wt_tnf[idx,1:5])
  pars = c(0,100)
  out<-runModel(nfkb_input = nfkb.rela,data.frame(k0=pars[1],k1=pars[2],
                                                  k2=lookupHalflife(gene)),times)
  (obj(pars))
  plot(out,type='l')
  points(mRNAprofile)
  idx<- which(out$time %in% mRNAprofile$time )
  resid <- out$mRNA[idx]-mRNAprofile$val
  plot(resid)
  sqrt(sum(resid^2)/length(resid))
  
  
  # (obj(out = out,mRNAprofile = mRNAprofile))
  # test fit 
  fit<-runOptim(initP = c(.1,.1),lb=0,ub = Inf,gene)
  plotFit(fit)
  
  # test fit list
  genelist <- c('Traf1','Upp1')
  load('./data/final_list.RData')
  result <- lapply(genelist,FUN = function(x)runOptim(initP = c(.1,.1),lb=0,ub = Inf,x))
}

## main code
main<-function(con){
  source('createHalflife.R')
  load("./data/chrinstine.RData")
  load("./data//hao.RData")
  load("./data/online.RData")
  #load(paste("./data/",deparse(substitute(con)),".RData",sep = ""))

  load(paste("./data/",deparse(substitute(deltaWTEV)),".RData",sep = "")) #detla data
  load(paste("./data/",deparse(substitute(wt_tnf)),".RData",sep = "")) #raw data 
  
  load("./data//nfkb.RData")
  load('./data/final_list.RData')
  
  times <- seq(0,8,by=0.01)
  
  require(deSolve)
  require(parallel)
  
  # check in all genes in halflife database 
  sum(tolower(GeneNames[final_list]) %in% rownames(online))/length(final_list)
  idx<-tolower(GeneNames[final_list]) %in% rownames(online)
  GeneNames[final_list[!idx]] # not found genes
  
  genelist<-GeneNames[final_list[idx]]
  
  
  numWorkers <- 12
  
  deltaDBDEV <- meanData[,11:15] - meanData[,1:5]
  
  results <- mclapply(genelist, function(x)runOptim(initP = c(.1,.1),lb=0,ub = Inf,x,deltaDBDEV), mc.cores = numWorkers)
  #results <- mclapply(genelist, function(x)runOptim(initP = c(.1,.1),lb=0,ub = Inf,x,wt_tnf), mc.cores = numWorkers)
  rsq<- sapply(1:length(results),function(x)results[[x]]$rsquared)
  ord_idx <- order(rsq,na.last = NA)
  save(file=paste("./data/",deparse(substitute(wt_tnf)),"resultsKD.RData",
                  sep=""),results,rsq,ord_idx)
#  save(file=paste("./data/",deparse(substitute(deltaWTEV)),"resultsKD.RData",
#                  sep=""),results,rsq,ord_idx)
  
  results
}


## main code
main2<-function(){
  source('createHalflife.R')
  load("./data/chrinstine.RData")
  load("./data//hao.RData")
  load("./data/online.RData")
  #load(paste("./data/",deparse(substitute(con)),".RData",sep = ""))
  
  load(paste("./data/",deparse(substitute(deltaWTEV)),".RData",sep = "")) #detla data
  load(paste("./data/",deparse(substitute(wt_tnf)),".RData",sep = "")) #raw data 
  
  load("./data//nfkb.RData")
  load('./data/final_list.RData')
  
  times <- seq(0,8,by=0.01)
  
  require(deSolve)
  require(parallel)
  
  # check in all genes in halflife database 
  sum(tolower(GeneNames[final_list]) %in% rownames(online))/length(final_list)
  idx<-tolower(GeneNames[final_list]) %in% rownames(online)
  GeneNames[final_list[!idx]] # not found genes
  
  genelist<-GeneNames[final_list[idx]]
  
  load(file="./data/wt_tnfresultsKD.RData")
  
  numWorkers <- 12
  results2 <- mclapply(genelist, function(x) runOptim(initP = results[[names(x)]]$par,
                                                    lb=0,ub = Inf,x,deltaWTEV), mc.cores = numWorkers)
  #results <- mclapply(genelist, function(x)runOptim(initP = c(.1,.1),lb=0,ub = Inf,x,wt_tnf), mc.cores = numWorkers)
  rsq<- sapply(1:length(results2),function(x)results2[[x]]$rsquared)
  ord_idx <- order(rsq,na.last = NA)
  save(file=paste("./data/",deparse(substitute(wt_tnf)),"resultsKD2.RData",
                  sep=""),results2,rsq,ord_idx)
  results
}


## run optim 
runOptimEV_wrapper <- function(initP){
  
}

