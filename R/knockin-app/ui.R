library(shiny)
shinyUI(pageWithSidebar(
  headerPanel("Knockin RNAseq Data"),

  sidebarPanel(
    h3('Introduction'), 
    p("This application can predict your next word"),
    h3('Input box'), 
    p("Type any words seperated by space"),
    h3('Submit button'),
    p("Click the Submit button, then the app will tell you the next word")

    ),
  mainPanel(

    textInput("txtinput", label=h3("Text Input here:"), ""),
    br(),
    actionButton("goButton", "Submit"),
    
    h3("The next word is"),
    verbatimTextOutput("txtoutput")
    #h3("Examples")
    
  )
))