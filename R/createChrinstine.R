# create christine.RData

# rows: gene name, col1: half-life (h), col2: kdeg (-h)


# TNF condition only 
halflife<-c(0.2,0.3,0.3,0.6,.7,0.2,1.2)
gene<-c('TNF','Cxcl2','Cxcl1','Il6','Il1b','TTP','Zc3h12a')

chrinstine <- data.frame(gene=gene,halflife=halflife,kdeg=log(2)/halflife)

save(file='./data/chrinstine.RData',chrinstine)


