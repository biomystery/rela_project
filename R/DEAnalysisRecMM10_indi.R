########################################################
# DE analysis for Rec: EV, WT, DBD
### 1. prepare the data 
########################################################
rm(list=ls())
dataFolder <- '~/Dropbox/Projects/Rela_project/data/'
rawCnt <- read.csv(file=paste0(dataFolder,'rawCountRecAllmm10.csv'),stringsAsFactors = F)
rownames(rawCnt)<- rawCnt$X; rawCnt$X <- NULL 
colnames(rawCnt)


########################################################
### 2 DESeq2 analysis 
########################################################
col_data <- data.frame(genotype = as.factor(c(unlist(lapply(c('WT','DBD',"EV","TAD"), function(x) rep(x,5))),
                                              unlist(lapply(c('EV','DBD',"TAD","TAD1","TAD2","WT","TAD12"), function(x) rep(x,5))))),
                       timepoint = rep(c(0,.5,1,3,8),11),
                       replicate = c(rep('r1',20),rep('r2',35))
)
rownames(col_data) <- colnames(rawCnt)
col_data$timepoint <- as.factor(col_data$timepoint)
col_data$timepoint
col_data$genotype <- relevel(col_data$genotype,'WT')
col_data; dim(col_data) # 55 samples 

library(DESeq2)
dds <- DESeqDataSetFromMatrix(countData = rawCnt,
                              colData = col_data,
                              design = ~ genotype + timepoint)

dds <- dds[rowSums(counts(dds))>1,] # change threshold
dds <- estimateSizeFactors(dds)
dds <- estimateDispersions(dds)
normCnt <- counts(dds,normalized = T)
all.equal(normCnt[,1], counts(dds)[,1]/sizeFactors(dds)[1])
colData(dds)
design(dds)

########################################################
## Check rep1 & rep2 individually 
# http://seqanswers.com/forums/showthread.php?t=31036
# the p value is misleading
########################################################
getInduced <- function(batch ='r1',lfc_th = 1,geno = "WT"){
  ddsWT <- dds[,dds$genotype == geno & dds$replicate == batch]
  design(ddsWT) <- ~ timepoint
  rld <- rlogTransformation(ddsWT)
  
  res <- data.frame(  
    assay(rld), 
    avgLogExpr.5 = ( assay(rld)[,2] + assay(rld)[,1] ) / 2,
    rLogFC.5 = assay(rld)[,2] - assay(rld)[,1], 
    avgLogExpr1= ( assay(rld)[,3] + assay(rld)[,1] ) / 2,
    rLogFC1 = assay(rld)[,3] - assay(rld)[,1], 
    avgLogExpr3 = ( assay(rld)[,4] + assay(rld)[,1] ) / 2,
    rLogFC3 = assay(rld)[,4] - assay(rld)[,1], 
    avgLogExpr8 = ( assay(rld)[,5] + assay(rld)[,1] ) / 2,
    rLogFC8 = assay(rld)[,5] - assay(rld)[,1] 
  )
  
  fc_indx <- grep('rLogFC',colnames(res))
  inducedGenes <- sapply(fc_indx, function(x) rownames(res)[res[,x]>=lfc_th])
  #install.packages("venn")
  library(venn)
  venn(inducedGenes)
  inducedGenes # 115 genes rlog >=1 vs wt 0 rep1
}


b1_induced <- getInduced()
b2_induced <- getInduced(batch = 'r2')

length(unique(unlist(b1_induced)))
length(unique(unlist(b2_induced)))

getRelA <- function(batch ='r1',lfc_th = -1,geno = c("WT","EV")){
  ddsWT <- dds[,dds$genotype %in% geno & dds$replicate == batch]
  rld <- rlogTransformation(ddsWT)
  
  res <- data.frame(  
    assay(rld), 
    avgLogExpr0 = ( assay(rld)[,6] + assay(rld)[,1] ) / 2,
    rLogFC0 = assay(rld)[,6] - assay(rld)[,1], 
    avgLogExpr0 = ( assay(rld)[,7] + assay(rld)[,2] ) / 2,
    rLogFC.5 = assay(rld)[,7] - assay(rld)[,2], 
    avgLogExpr1= ( assay(rld)[,8] + assay(rld)[,3] ) / 2,
    rLogFC1 = assay(rld)[,8] - assay(rld)[,3], 
    avgLogExpr3 = ( assay(rld)[,9] + assay(rld)[,4] ) / 2,
    rLogFC3 = assay(rld)[,9] - assay(rld)[,4], 
    avgLogExpr8 = ( assay(rld)[,10] + assay(rld)[,5] ) / 2,
    rLogFC8 = assay(rld)[,10] - assay(rld)[,5] 
  )
  
  fc_indx <- grep('rLogFC',colnames(res))
  if(batch =='r1')
    inducedGenes <- sapply(fc_indx, function(x) rownames(res)[res[,x]<=lfc_th])
  else
    inducedGenes <- sapply(fc_indx, function(x) rownames(res)[res[,x]>=abs(lfc_th)])

  #install.packages("venn")
  library(venn)
  venn(inducedGenes)
  inducedGenes # 115 genes rlog >=1 vs wt 0 rep1
}
b1_rela_gene <- getRelA()
b2_rela_gene <- getRelA(batch = 'r2')

b2_genes <- intersect(unlist(b2_rela_gene),unlist(b2_induced))
colnames(normCnt)

b2_data <- normCnt[b2_genes,col_data$replicate=="r2"]
write.csv(file=paste0(dataFolder,'b2_rela_normcount.csv'),b2_data)

dds_b2 <- dds[, dds$replicate == 'r2']
b2_rld <- rlogTransformation(dds_b2)
b2_rld <- b2_rld[b2_genes,]
b2_rld <- assay(b2_rld)
write.csv(file=paste0(dataFolder,'b2_rlog.csv'),b2_rld)

#plot b2_rlog ------
dataFolder <- '~/Dropbox/Projects/Rela_project/data/'

b2_rld <- read.csv(file=paste0(dataFolder,'b2_rlog.csv'),
                   stringsAsFactors = F,row.names = 1)
colnames(b2_rld)

col_data <- subset(col_data,replicate=='r2')
col_data

require(pheatmap)
pheatmap(b2_data,scale = 'row',cluster_cols = F,
         color = colorRampPalette(c("blue","white",'red'))(20))

pd <-b2_rld- b2_rld[,'WT_0.0_r2']
#pd[pd> -min(pd)] <- -min(pd)

colnames(pd)[col_data$genotype=="WT"]


set.seed(10)
kpd <- kmeans(x = pd[,col_data$genotype=="WT"],centers = 8,nstart = 20)
#dev.off()
kord <- c(2,3,5,4,1,7,6,8)
#kord <- 1:8
roder<- unlist(sapply(kord,function(i) which(kpd$cluster==i)))
rbks <-unlist(sapply(kord,function(i)
  which(kpd$cluster==i)[sum(kpd$cluster==i)]))
rbks <- which(roder %in% rbks)
#pd[pd>2]<- 2
corder <- c('WT','EV',"DBD","TAD","TAD1","TAD2","TAD12")
corder <- unlist(sapply(1:7, function(i)
  which(col_data$genotype ==corder[i])))
pd2 <- pd[roder,corder]
pheatmap(pd2,#scale = 'row',
         breaks = c(seq(min(pd),-min(pd),length.out = 41),max(pd)),
         cluster_rows = F,
         cluster_cols = F,show_rownames = F,
         gaps_row =  rbks,
         gaps_col = grep('8.0',colnames(pd)),
         color = c(colorRampPalette(c("blue","white",'red'))(41),'red'))
dev.copy2pdf(file=paste0(dataFolder,'b2.pdf'))

calPhenoscore <- function(geo ='DBD'){
  #"DBD","TAD","TAD1","TAD2","TAD12"
  wt <- pd2[,"WT_0.0_r2"]
  ev <- pd2[,"EV_0.0_r2"]
  mt <- pd2[,paste0(geo,'_0.0_r2')]
  #wt[1]
  #ev[1]
  #mt[1]
  (mt-ev)/(wt-ev)
}

phenoscores <- sapply(c("DBD","TAD","TAD1","TAD2","TAD12"), calPhenoscore)
phenoscores
phenoscores[phenoscores<0] <- 0
phenoscores[phenoscores>1] <- 1
pheatmap(phenoscores[roder,],
         cluster_rows = F,
         cluster_cols = F,show_rownames = F,
         gaps_row =  rbks)

##plot--------
pdf(paste0(dataFolder,'mm10_AddedTechniqueRep_analysis.pdf'))
venn(b1_induced,snames = c("b1_0.5h",'b1_1h','b1_3h','b1_8h'),ellipse =T)
venn(b2_induced,snames = c("b2_0.5h",'b2_1h','b2_3h','b2_8h'),ellipse =T)

venn(list(wt_induced_b1 = unique(unlist(b1_induced)),
          wt_induced_b2=  unique(unlist(b2_induced))))

venn(list(wt_induced_b1 = unique(unlist(b1_induced)),
          EV_reduced_b1 = unique(unlist(b1_rela_gene))))

venn(list(wt_induced_b2 = unique(unlist(b2_induced)),
          EV_reduced_b2 = unique(unlist(b2_rela_gene))))

pheatmap(b2_lfc,cluster_cols = F,gaps_col = c(5,10),
         scale = 'none',treeheight_row = 0,show_rownames = F ,
         main=paste0("b2, lfc to wt0 for ",nrow(b2_lfc),' genes'))

dev.off()

# batch 2 ------

geno <- c("WT","EV","DBD")
ddsWT <- dds[,dds$genotype %in% geno & dds$replicate == 'r1']
rld <- rlogTransformation(ddsWT)
b2_rld <- assay(rld)

b2_genes <- intersect(unique(unlist(b1_induced)),unique(unlist(b1_rela_gene)))
b2_rld <- b2_rld[b2_genes,]
b2_lfc <- b2_rld- b2_rld[,"WT_0.0_r1"]
library(pheatmap)
pheatmap(b2_lfc,cluster_cols = F,gaps_col = c(5,10),
         scale = 'none',treeheight_row = 0,show_rownames = T ,
         main=paste0("b1, lfc to wt0 for ",nrow(b2_lfc),' genes'))
dev.copy2pdf(file='r1.pdf')
library("org.Mm.eg.db")
rownames(b2_lfc)<- as.character(mapIds(org.Mm.eg.db,
        keys=b2_genes,
        column="SYMBOL",
        keytype="ENSEMBL",
        multiVals="first") )
# focus on batch 2: the new mutants ------
colnames(normCnt)
col_data2 <- subset(col_data,replicate=='r2')
normCnt2 <- normCnt[,col_data$replicate=='r2']
colnames(normCnt2)

# load the 87 gene
r1_genes <- read.csv(file=paste0(dataFolder,'RNASeq-Rec-Rep1/processed/allLpsTNFdata.csv'),
                     stringsAsFactors = F)
rownames(r1_genes)[c(78,87)] # Pion (Gsap) & Gm17311
library("org.Mm.eg.db")
r1_genes<-as.character(mapIds(org.Mm.eg.db,
                    keys=rownames(r1_genes),
                    column="ENSEMBL",
                    keytype="SYMBOL",
                    multiVals="first") )
r1_genes[c(78,87)] <- c('ENSMUSG00000039934','ENSMUSG00000091113')
sum(rownames(rawCnt) %in% r1_genes)
rawCnt2 <- rawCnt[rownames(rawCnt) %in% r1_genes,]
r1_genes[!r1_genes%in% rownames(rawCnt2)]
dim(rawCnt2) ; length(unique(r1_genes))

plot_data <- normCnt[,col_data$replicate=='r2']
#plot_data <- rawCnt[,col_data$replicate=='r1']
#plot_data <- rep1[rownames(rep1) %in% r1_genes,]
#tmp<-sapply(1:ncol(plot_data),
#            function(x) plot_data[,x]/colSums(rep1)[x])
#tmp<-sapply(1:ncol(plot_data),
#            function(x) plot_data[,x]/colSums(plot_data)[x] *10^6)
#colnames(tmp)<- colnames(plot_data);rownames(tmp)<-rownames(plot_data)
#tmp <- tmp[rownames(tmp) %in% r1_genes,]

plot_data <- plot_data[rownames(plot_data) %in% r1_genes,]
rownames(plot_data) <- as.character(mapIds(org.Mm.eg.db,
                    keys=rownames(plot_data),
                    column="SYMBOL",
                    keytype="ENSEMBL",
                    multiVals="first") )

as.character(mapIds(org.Mm.eg.db,
                    keys='Tnf',
                    column="ENSEMBL",
                    keytype="SYMBOL",
                    multiVals="first"))

pheatmap(plot_data,cluster_cols = F,
         scale = 'row')
rownames(r1_genes)[!(rownames(r1_genes) %in% rownames(plot_data))]
id <- which(rownames(plot_data) == 'Gsap')
tmp <- rownames(plot_data)
tmp[id] <- 'Pion'; rownames(plot_data) <- tmp


write.csv(file=paste0(dataFolder,'batch2NormCnt86gene.csv'),plot_data)

## 
dev.copy2pdf(file='86_batch2_normCnt.pdf')
