addpath('~/Dropbox/matlab/')
clear;

load('~/Dropbox/Projects/Rela_project/data/countData.mat')


%% fold change to wt 1 
%%%%%%%%%%%%%%%%%%%%%
order = [6:10 1:5 11:50];
order = order([1:10 26:30]);
order = order([1:15]);
cgo = clustergram(countData.log2foldWT_1(:,order),'Cluster',1,'Colormap', redbluecmap);%,'Standarize','Row')

%%
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order))

%% plot log2fold_2 same order as log2fold_wt
%%%%%%%%%%%%%%%%%%%%%
order_wt = get(cgo,'RowLabels');
order_wt = cellfun(@str2num,order_wt);
hm1 = HeatMap(countData.log2fold(order_wt,order),...
    'DisplayRange',3,'Colormap',redbluecmap);
set(hm1,'ColumnLabels',countData.sample(order))

plot(hm1)
set(hm1,'RowLabels',countData.genes(order_wt),...
    'ColumnLabels',countData.sample(order))
%
%save2pdf('log2Fold_2')

%% plot2. hierechical DBD score
%%%%%%%%%%%%%%%%%%%%%
hm1 = HeatMap(countData.mutScoreFinal(order_wt,[16:20]),...
    'Symmetric',0,'Colormap',flipud(othercolor('PuOr5',21)));
%plot(hm1)
set(hm1,'RowLabels',countData.genes(order_wt),...
     'ColumnLabels',countData.sample(col_order([11:15])))

%% final pheno_score for all mutants/ TNF 
%%%%%%%%%%%%%%%%%%%%%
col_order = [6:10 1:5 26:30 11:25 31:50 ... 
    56:60 51:55 76:80 61:75 81:100];
 plot_data = countData.log2foldWT_1;
%  plot_data = countData.mutScoreFinal;

cgo = clustergram(plot_data(:,col_order),'Cluster',1,'Colormap',...
    redbluecmap);%,'Standarize','Row')
%
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(col_order))
%% replot log2 fold all  using imagesc
figure('position',[ 1    33   1280   672])
tmp_lab  = countData.sample(col_order); 
t_lab = tmp_lab(3:5:100);
for i = 1:20
    t_lab{i} = t_lab{i}(1:end-2);
    if(t_lab{i}(end) =='_') t_lab{i} = t_lab{i}(1:end-1); end
    if(strcmp(t_lab{i},'DBD')) t_lab{i} = 'DB'; end
end
imagesc(countData.log2foldWT_1(order_wt,col_order),[-3 3]);
set(gca,'ytick',1:87,'yticklabel',countData.genes(order_wt),'ydir','norm',...
    'xtick',3:5:100,'xticklabel',t_lab)
colormap(redbluecmap)
save2pdf('all_log2wt_20')

%%
figure('position',[ 1    33   1280*16/20   672])
tmp_lab  = countData.sample(col_order([11:50,61:100])); 
t_lab = tmp_lab(3:5:80);
for i = 1:16
    t_lab{i} = t_lab{i}(1:end-2);
    if(t_lab{i}(end) =='_') t_lab{i} = t_lab{i}(1:end-1); end
    if(strcmp(t_lab{i},'DBD')) t_lab{i} = 'DB'; end
end

imagesc(countData.mutScoreFinal(order_wt,[16:20 1:15 21:40 56:60 41:55 61:80]),[0 1]);
set(gca,'ytick',1:87,'yticklabel',countData.genes(order_wt),'ydir','norm',...
    'xtick',3:5:80,'xticklabel',t_lab)
colormap(flipud(othercolor('PuOr5',21)))
save2pdf('all_phenoscore_21')

%%

%%

order_wt = get(cgo,'RowLabels');
order_wt = cellfun(@str2num,order_wt);


%%
hm1 = HeatMap(countData.mutScoreFinal(order_wt,[16:20 1:15 21:40 56:60 41:55 61:80]),...
    'Symmetric',0,'Colormap',flipud(othercolor('PuOr5',21)));
%plot(hm1)
set(hm1,'RowLabels',countData.genes(order_wt),...
     'ColumnLabels',countData.sample(col_order([11:50,61:100])))
% set(hm1,'RowLabels',countData.genes(order_wt),...
%     'ColumnLabels',countData.sample(order))

%% load batch2 data 
batchTab = readtable('~/Dropbox/Projects/Rela_project/data/batch2NormCnt86gene.csv');
%%
countData2.genes = batchTab.Var1;
countData2.ncount = batchTab(:,2:36); 
countData2.mutants = {'EV','DB','TAD','TAD1','TAD2','WT','TAD12'};
%% calculate log2 fold
countData2.log2fold = table2array(countData2.ncount) + 0.5; 
tmp = countData2.log2fold;
for cid = 1:35
    countData2.log2fold(:,cid)= log2(countData2.log2fold(:,cid)./tmp(:,26));
end

%% calculate phenoscore 
% (x-ev)/wt
countData2.mutPhenoScore = zeros(86,25);
countData2.mutSample = countData2.mutants([2:5,7]);
gids = [6,11,16,21,31]; % DB, TAD, TAD1, TAD2,TAD12
for gid = 1:5
    for tid = 1:5
        countData2.mutPhenoScore(:,(gid-1)*5+tid) = (tmp(:,gids(gid)+tid-1)- tmp(:,tid))./tmp(:,26+tid-1);
    end
end
countData2.mutPhenoScore(countData2.mutPhenoScore>1) = 1;
countData2.mutPhenoScore(countData2.mutPhenoScore<0) = 0;


%% plot the data 
cgo= clustergram(countData2.log2fold,'Cluster',1,'Colormap', redbluecmap);%,'Standarize','Row')
%%
countData2.sample = batchTab.Properties.VariableNames(2:end);
set(cgo,'RowLabels',countData2.genes,'ColumnLabels',countData2.sample)

%%
%%
hm1 = HeatMap(countData2.mutPhenoScore,...
    'Symmetric',0,'Colormap',flipud(othercolor('PuOr5',21)));

%% order 
countData.genes(order_wt)

% construct a name matrix 
tmp2 = cell(86,2);
tmp2(:,1) = countData.genes(order_wt(order_wt ~= 87));
tmp2(:,2) = countData2.genes(1:86);

%% find out the order in batch2
order_bat2 = zeros(86,1) ;
for i =1:86
    [ ~, order_bat2(i)] = ismember(tmp2(i,1),tmp2(:,2));
end

%% plot log2_fold 
mt_order = [6 1:5,7];
mt_cl_order = 1:35; 
for i = 1:7 
    mt_cl_order(((i-1)*5+1):(i*5)) = gen_cl_order(mt_order(i));
end 
%% 
gen_cl_order = @(x) ((x-1)*5+1):(x*5);
figure('position',[ 1    33   640   672])
imagesc(countData2.log2fold(order_bat2,mt_cl_order),[-3 3]);
set(gca,'ytick',1:86,'yticklabel',countData2.genes(order_bat2),'ydir','norm',...
    'xtick',1:5:35,'xticklabel',countData2.mutants(mt_order))
colormap(redbluecmap)
save2pdf('b2_log2fold.pdf')
%
figure('position',[ 1    33   640   672])
imagesc(countData2.mutPhenoScore(order_bat2,:),[0 1]);
set(gca,'ytick',1:86,'yticklabel',countData2.genes(order_bat2),'ydir','norm',...
    'xtick',1:5:25,'xticklabel',countData2.mutSample)
colormap(flipud(othercolor('PuOr5',21)))
save2pdf('b2_phenoscore.pdf')

%%
mt_order = [6 1 3:5,7];
mt_cl_order = 1:30; 
for i = 1:6
    mt_cl_order(((i-1)*5+1):(i*5)) = gen_cl_order(mt_order(i));
end 

gen_cl_order = @(x) ((x-1)*5+1):(x*5);
figure('position',[ 1    33   640   672])
imagesc(countData2.log2fold(order_bat2,mt_cl_order),[-3 3]);
set(gca,'ytick',1:86,'yticklabel',countData2.genes(order_bat2),'ydir','norm',...
    'xtick',1:5:30,'xticklabel',countData2.mutants(mt_order))
colormap(redbluecmap)
%%
mt_order = [6 1 3 7];
gen_cl_order = @(x) ((x-1)*5+1):(x*5);
mt_cl_order = 1:20; 

for i = 1:4
    mt_cl_order(((i-1)*5+1):(i*5)) = gen_cl_order(mt_order(i));
end 


figure('position',[ 1    33   640/3*2   672])
imagesc(countData2.log2fold(order_bat2,mt_cl_order),[-3 3]);
set(gca,'ytick',1:86,'yticklabel',countData2.genes(order_bat2),'ydir','norm',...
    'xtick',1:5:30,'xticklabel',countData2.mutants(mt_order))
colormap(redbluecmap)
save2pdf('b2_log2fold_a1')


%%
mt_order = [6 1 4 5 7];
gen_cl_order = @(x) ((x-1)*5+1):(x*5);
mt_cl_order = 1:25; 

for i = 1:5
    mt_cl_order(((i-1)*5+1):(i*5)) = gen_cl_order(mt_order(i));
end 


figure('position',[ 1    33   640/30*25   672])
imagesc(countData2.log2fold(order_bat2,mt_cl_order),[-3 3]);
set(gca,'ytick',1:86,'yticklabel',countData2.genes(order_bat2),'ydir','norm',...
    'xtick',1:5:30,'xticklabel',countData2.mutants(mt_order))
colormap(redbluecmap)
save2pdf('b2_log2fold_a2')

%%
figure('position',[ 1    33   640   672])
imagesc(countData2.mutPhenoScore(order_bat2,6:end),[0 1]);
set(gca,'ytick',1:86,'yticklabel',countData2.genes(order_bat2),'ydir','norm',...
    'xtick',1:5:20,'xticklabel',countData2.mutSample(2:end))
colormap(flipud(othercolor('PuOr5',21)))
save2pdf('b2_phenoscore_2.pdf')

%% plot examples 
log2fold.ncount = table2array(countData2.ncount(order_bat2,mt_cl_order));
log2fold.value = countData2.log2fold(order_bat2,mt_cl_order); 
log2fold.genes = countData2.genes(order_bat2);
log2fold.mutants = countData2.mutants(mt_order);



%%

%%

plot([0 0.5 1 3 8], reshape(log2fold.value(1,:),5,6),'-o')

%%
log2fold.value(1,1:5)
reshape(log2fold.value(1,:),6,5)

%% 
cols = othercolor('Bu_10',6);
cols = [.15 .15 .15;
    .75 .75 .75;
    [139 0 139]/255;
    [0 191 255]/255;
    [205 92 92]/255;
    [255 131 250]/255
    
    ];
set(groot,'defaultAxesColorOrder',cols)

for i = 1:86
figure('position',[641   408   254   191])

plot([0 0.5 1 3 8], reshape(log2fold.ncount(i,:),5,6),'-o',...
    'linewidth',1.5)
set(gca,'xtick',[0 0.5 1 3 8])
xlabel('Time (hr)');
ylabel('Expression (Cpm)');
title(log2fold.genes(i))
grid on
if i ==1
    legend(log2fold.mutants,'location','best')
end
save2pdf(log2fold.genes{i})
close 
end
