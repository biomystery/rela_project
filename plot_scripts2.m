clear;
addpath(genpath('~/Dropbox/matlab/'))
dataFolder = '~/Dropbox/Kim/data/old/';

%% load the data & make data file
ncount = readtable([dataFolder,'allLpsTNFdataAll.csv']);
% samples
countData.ncount = ncount{:,2:end}

countData.genes = ncount{:,1};
countData.sample = ncount.Properties.VariableNames(2:end)

tmp  = {'EV0h','EV0.5h','EV1h','EV3h','EV8h',...
    'WT0h','WT0.5h','WT1h','WT3h','WT8h',...
    'TAD0h','TAD0.5h','TAD1h','TAD3h','TAD8h',...
    'TAD2_0h','TAD2_0.5h','TAD2_1h','TAD2_3h','TAD2_8h',...
    'TAD1_0h','TAD1_0.5h','TAD1_1h','TAD1_3h','TAD1_8h',...
    'DBD0h','DBD0.5h','DBD1h','DBD3h','DBD8h',...
    'S276A0h','S276A0.5h','S276A1h','S276A3h','S276A8h',...
    '3A0h','3A0.5h','3A1h','3A3h','3A8h',...
    '4A0h','4A0.5h','4A1h','4A3h','4A8h',...
        'K310R0h','K310R0.5h','K310R1h','K310R3h','K310R8h'
    }

tmp2  = cellfun(@(x) [x,'lps'],tmp,'UniformOutput',false);
countData.sample ={tmp{:}, tmp{:}};
countData.log2count = log2(countData.ncount)


% all mutant score
normaliz=@(x,ev,wt) (x-ev)./(wt-ev);

mutPhenoScore1 = cell2mat( arrayfun(@(x)normaliz(countData.ncount(:,x:(x+4)),...
    countData.ncount(:,1:5),countData.ncount(:,6:10)),...
    11:5:50,'UniformOutput',false))

mutPhenoScore2 = cell2mat( arrayfun(@(x)normaliz(countData.ncount(:,[x:(x+4)]),...
    countData.ncount(:,[1:5]+50),countData.ncount(:,[6:10]+50)),...
    (11:5:50)+50,'UniformOutput',false))

% score
countData.mutPhenoScore_n = [mutPhenoScore1  mutPhenoScore2];
countData.mutPhenoScore_n(countData.mutPhenoScore_n<0) = 0;
countData.mutPhenoScore_n(countData.mutPhenoScore_n>1) = 1;

countData.mutants= {'TAD','TAD2','TAD1','DBD','S276A','3A','4A','K310R',...
    'TAD','TAD2','TAD1','DBD','S276A','3A','4A','K310R'};

%% mutant Score * Ev Score 
countData.evScore = countData.ncount(:,[1:5 51:55])./countData.ncount(:,[6:10 56:60]);
countData.evScore(countData.evScore>1) = 1;
%
countData.mutScoreFinal = countData.mutPhenoScore_n;
countData.mutScoreFinal(:,1:40) = cell2mat(arrayfun(@(x) ...
    countData.mutPhenoScore_n(:,x:(x+4)).*(1-countData.evScore(:,1:5)),...
    1:5:40,'UniformOutput',false)); % TNF

countData.mutScoreFinal(:,41:80) = cell2mat(arrayfun(@(x) ...
    countData.mutPhenoScore_n(:,x:(x+4)).*(1-countData.evScore(:,6:10)),...
    41:5:80,'UniformOutput',false)); % TNF


% fold 
countData.log2fold = countData.ncount ;
countData.log2foldWT = countData.ncount ;

for i =1:numel(countData.sample)
    countData.log2fold(:,i) = log2(countData.ncount(:,i)./countData.ncount(:,floor((i-1)/5)*5+1));
    countData.log2foldWT(:,i) = log2(countData.ncount(:,i)./countData.ncount(:,6));
end
%
countData.ncount_1 = countData.ncount+1;
countData.log2fold_1 =countData.ncount_1;

for i =1:numel(countData.sample)
    countData.log2fold_1(:,i) = log2(countData.ncount_1(:,i)./...
        countData.ncount_1(:,floor((i-1)/5)*5+1));
    countData.log2foldWT_1(:,i) = log2(countData.ncount_1(:,i)./...
        countData.ncount_1(:,6));
end

% save data
%
save('countData.mat','countData')


%% Plot 1:  Pheno_score plot for DBD only 
order = [16:20 1:15 21:40]; 
order = [order order+40]; 
order1 = [26:30 11:25 31:50]; 
order1 = [order1 order1+50]; 

cols = 1:80; % tnf 41:80;%LPS 1:80; all
plot_data = countData.mutScoreFinal;

cgo = clustergram(plot_data(:,order(cols(1:5))),'DisplayRange',1,'Cluster',1,'Colormap',...
    flipud(othercolor('RdBu4',21)),'Symmetric','f');%,'Standarize','Row') %PuOr5
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order1(cols(1:5))))

%%
save2pdf('Pheno_score_DBD')

%% Plot 1:  Pheno_score plot for all mutants/ TNF 
order = [16:20 1:15 21:40]; 
order = [order order+40]; 
order1 = [26:30 11:25 31:50]; 
order1 = [order1 order1+50]; 

cols = 1:80; % tnf 41:80;%LPS 1:80; all
plot_data = countData.mutScoreFinal;

cgo = clustergram(plot_data(:,order(cols)),'Cluster',1,'Colormap',...
    flipud(othercolor('PuOr4',21)),'Symmetric','f');%,'Standarize','Row') %PuOr5
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order1(cols)))

%% plot2 . hierechical Log2fold or Log2FoldWt
order = [6:10 1:5 26:30 11:25 31:50];
order = [order order+50];
cm = struct('GroupNumber', 1:50,...
    'Annotation', {'Time1', 'Time2'},...
    'Color', {[1 1 0], [0.6 0.6 1]});
plot_data = countData.log2foldWT_1(:,order);
tmp = plot_data(:,51);
for i =51:100
plot_data(:,i) = plot_data(:,i)-tmp;
end
cgo = clustergram(plot_data,'Cluster',1,'Colormap', redbluecmap(11));%,'Standarize','Row')
%
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order));
 

%% 
plot_data2 = arrayfun(@(x) max(plot_data(:,x:(x+4)),[],2),1:5:100,...
    'UniformOutput',false);
samples = {'WT','EV','DBD','TAD','TAD2','TAD1','S276A','3A','4A','k310R'};
samples2 = {'WT_TNF','EV_TNF','WT_LPS','EV_LPS'};
plot_data2=cell2mat(plot_data2);
ids = [1:2 11:12];
cgo = clustergram(plot_data2(:,ids),'Colormap', redbluecmap(11));%,'Standarize','Row')
set(cgo,'RowLabels',countData.genes,'ColumnLabels',samples2);

%% show fold induction 


%%
save2pdf('all_mutants_log2wt')
%%
tree = linkage(countData.log2foldWT_1(:,order),'average');
D = pdist(countData.log2foldWT_1(:,order));
leafOrder = optimalleaforder(tree,D);

%%
close all
figure('Position',[ -680   559   148   419])

dendrogram(tree,0,'Reorder',leafOrder,...
    'Labels',countData.genes,...
    'Orientation', 'left')
%
axis off
%%
save2pdf('dendrogram_wt')
%%
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order))
%set(cgo,'ColumnLabels',countData.sample(order))%,'RowGroupMarker', rm)
plot(cgo)
%% plot log2fold_2 same order as log2fold_wt
order_wt = get(cgo,'RowLabels');
order_wt = cellfun(@str2num,order_wt);
hm1 = HeatMap(countData.log2fold(order_wt,order),...
    'DisplayRange',3,'Colormap',redbluecmap);
set(hm1,'ColumnLabels',countData.sample(order))

plot(hm1)
set(hm1,'RowLabels',countData.genes(order_wt),...
    'ColumnLabels',countData.sample(order))
%
%save2pdf('log2Fold_2')


%%
for i =1:ncluster
    imagesc([countData.log2fold_1(idx==n,[7:10,2:5,12:15]);...
        countData.log2fold_1(idx==1,[7:10,2:5,12:15]);...
        countData.log2fold_1(idx==2,[7:10,2:5,12:15])],[-3,3])
end

%%
colorbar
%%
save2pdf('log2fold')
!open log2fold.pdf

%% Cmp old data and new data
load ./countData.mat
countDataNew = countData;
load ./countData_old.mat
countDataOld = countData;
clear countData

%%
countDataNew.mutants
countDataOld.mutants
%% DBD
countDataNew.ncount(1:10,26:30)
countDataOld.ncount(1:10,11:15)

%% score DBD
countDataOld.mutPhenoScore_n(1:10,1:5)
countDataNew.mutPhenoScore_n(1:10,16:20)
