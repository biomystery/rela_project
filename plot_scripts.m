addpath('~/Dropbox/matlab/')
clear;
%% load the data & make data file
ncount = readtable('./data/final_list.csv');
countData.ncount = ncount{:,2:end}

countData.genes = ncount{:,1};
countData.sample = ncount.Properties.VariableNames(2:end)
%%
countData.sample = {'EV0h','EV0.5h','EV1h','EV3h','EV8h',...
    'WT0h','WT0.5h','WT1h','WT3h','WT8h',...
      'DBD0h','DBD0.5h','DBD1h','DBD3h','DBD8h',...
    'TAD0h','TAD0.5h','TAD1h','TAD3h','TAD8h',...
    'TAD2_0h','TAD2_0.5h','TAD2_1h','TAD2_3h','TAD2_8h',...
    'TAD1_0h','TAD1_0.5h','TAD1_1h','TAD1_3h','TAD1_8h',...
    'S276A0h','S276A0.5h','S276A1h','S276A3h','S276A8h',...
    '3A0h','3A0.5h','3A1h','3A3h','3A8h',...
    '4A0h','4A0.5h','4A1h','4A3h','4A8h',...
        'K310R0h','K310R0.5h','K310R1h','K310R3h','K310R8h'
    }
%
countData.log2count = log2(countData.ncount)

%% EV pheno score vs. wt
countData.evScore = countData.ncount(:,[1:5])./countData.ncount(:,[6:10])


%%
tmp =countData.ncount(:,[6:10])./ countData.ncount(:,[1:5]);
[rowmax,~]=min(tmp,[],2)

%%
sum(rowmax>0.5)
%% dbd pheno score vs. wt
countData.DBDScore = countData.ncount(:,[11:15])./countData.ncount(:,[6:10])
%
countData.mutScore = cell2mat(arrayfun(@(x) countData.ncount(:,[x:(x+4)])./countData.ncount(:,[6:10])...
    ,[1,11:5:50],'UniformOutput',false))
%%
% dbd pheno score vs. Ev and wt
normaliz=@(x,ev,wt) (x-ev)./(wt-ev);
countData.DBDPhenoScore = normaliz(countData.ncount(:,[11:15]),...
    countData.ncount(:,[1:5]),countData.ncount(:,[6:10]));
countData.DBDPhenoScore_n = countData.DBDPhenoScore;
countData.DBDPhenoScore_n(countData.DBDPhenoScore<0) = 0;
countData.DBDPhenoScore_n(countData.DBDPhenoScore>1) = 1;
%%

countData.mutPhenoScore = cell2mat( arrayfun(@(x)normaliz(countData.ncount(:,[x:(x+4)]),...
    countData.ncount(:,[1:5]),countData.ncount(:,[6:10])),...
    11:5:50,'UniformOutput',false))
%
countData.mutPhenoScore_n = countData.mutPhenoScore;
countData.mutPhenoScore_n(countData.mutPhenoScore_n<0) = 0;
countData.mutPhenoScore_n(countData.mutPhenoScore_n>1) = 1;

countData.mutants= {'DBD','TAD','TAD2','TAD1','S276A','3A','4A','K310R'};
%%
countData.log2fold = countData.ncount ;
countData.log2foldWT = countData.ncount ;

for i =1:numel(countData.sample)
    countData.log2fold(:,i) = log2(countData.ncount(:,i)./countData.ncount(:,floor((i-1)/5)*5+1));
    countData.log2foldWT(:,i) = log2(countData.ncount(:,i)./countData.ncount(:,6));
end
%
countData.ncount_1 = countData.ncount+1;
countData.log2fold_1 =countData.ncount_1;

for i =1:numel(countData.sample)
    countData.log2fold_1(:,i) = log2(countData.ncount_1(:,i)./...
        countData.ncount_1(:,floor((i-1)/5)*5+1));
    countData.log2foldWT_1(:,i) = log2(countData.ncount_1(:,i)./...
        countData.ncount_1(:,6));
end

%% save data

countData.DBDScoreFinal = countData.DBDPhenoScore_n.*(1-countData.evScore);
countData.mutScoreFinal = cell2mat(arrayfun(@(x) ...
    countData.mutPhenoScore_n(:,x:(x+4)).*(1-countData.evScore),...
    1:5:40,'UniformOutput',false));
%%
save('countData.mat','countData')

%%%%%%%%%%%%%%%%%%%%%%
%% plot
% % compare EVvsWT vs. DBDvsWT at Basal
% close all
% figure('position', [-974   555   560/5   420])
% imagesc(sortrows(countData.log2foldWT(:,[1,11])))
% set(gca,'yticklabel','','xticklabel',{'EV','DBD'})
% colormap(redgreencmap)
% colorbar
% 
% %% plot 1. heatmap of fold induction
% close all;
% figure('position', [-974   555   560   420])
% imagesc(countData.log2fold(:,[2:5,7:10,12:15]),[-2,2]);
% colormap(redgreencmap)
% colorbar


%% plot2. hierechical for ev score
close all;
cgo = clustergram(countData.evScore,'Cluster',1,'Colormap', flipud(othercolor('PuOr3',21)),'Symmetric','f');%,'Standarize','Row')
%
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(1:5))
%
plot(cgo)
%
%save2pdf('ev')

%% 
imagesc(countData.mutScoreFinal(order_wt,:),[0 1])
colormap(flipud(othercolor('PuOr3',21)))


%% other orders 
figure
imagesc(countData.evScore(ev_idx,:),[0 1])
colormap(flipud(othercolor('PuOr3',21)))
%%
imagesc(countData.DBDPhenoScore_n(countData.rela_idx,:),[0 1])
colormap(flipud(othercolor('PuOr3',21)))

%% plot2. hierechical DBD score
close all;
cgo = clustergram(countData.DBDPhenoScore_n(countData.rela_idx,:),'Cluster',1,'Colormap',...
    redgreencmap(21,'INTERPOLATION','linear'),'Symmetric','f');%,'Standarize','Row')

set(cgo,'RowLabels',countData.genes(dbd_idx,:),'ColumnLabels',countData.sample(11:15))

plot(cgo)

save2pdf('dbd2')

%% final pheno_score for all mutants/ TNF 

plot_data = countData.log2;
%  plot_data = countData.mutScoreFinal;

cgo = clustergram(plot_data,'Cluster',1,'Colormap',...
    flipud(othercolor('PuOr5',21)),'Symmetric','f');%,'Standarize','Row')

%%
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample([11:50 61:100]))
%%
plot(cgo)
%%
save2pdf('all_mutants_score')
%%
save2pdf('all_mutants_log2wt')
%%
save2pdf('DBD_mutants_score')
%%
colors = othercolor('Set19',10); 

plot_gene = @(gname)(arrayfun(@(x)(plot(countData.time,...
    countData.ncount(find(ismember(countData.genes,gname)),x:(x+4)),...
    'linewidth',2,'color',colors((x-1)/5+1,:))),1:5:50)); 

figure; 
for i =1:9
subplot(3,3,i)
hold on 
plot_gene(countData.cluster1{i})
title(countData.cluster1{i})
end

legend({'EV','wt',countData.mutants{1:end}},'location','best')

%%
save2pdf('cluster1_tc')

%%
idx =find(ismember(countData.genes,'Upp1'))
countData.evScore(idx,:)
%% plot3. hierechical Log2fold or Log2FoldWt
load('countData.mat')

order = [6:10 1:5 11:50];
order = order([1:10 21:25]);

cgo = clustergram(countData.log2foldWT_1(dbd_idx,order),'Cluster',1,'Colormap', redbluecmap);%,'Standarize','Row')
set(cgo,'RowLabels',countData.genes(dbd_idx,:),'ColumnLabels',countData.sample(order))

%%
countData.sample(order)
%%
% %% adding number 
% 
% [rows,cols] = size(plot_data);
% for i = 1:rows
%     for j = 1:cols
%         textHandles(j,i) = text(j,i,num2str(round(plot_data(i,j)*10)/10),...
%             'horizontalAlignment','center');
%     end
% end
%%

%% plot3. hierechical Log2fold or Log2FoldWt
order = [6:10 1:5 11:50];
order = order([1:10 26:30]);
order = order([1:15]);
cgo = clustergram(countData.log2foldWT_1(:,order),'Cluster',1,'Colormap', redgreencmap);%,'Standarize','Row')

set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order))
%%
tree = linkage(countData.log2foldWT_1(:,order),'average');
D = pdist(countData.log2foldWT_1(:,order));
% leafOrder = optimalleaforder(tree,);

%%
close all
figure('Position',[ -680   559   148   419])

dendrogram(tree,0,'Reorder',leafOrder,...
    'Labels',countData.genes,...
    'Orientation', 'left')
%
axis off
%%
save2pdf('For_b_cell_grant_103015_Frank.pdf')
%%
save2pdf('dendrogram_wt')
%%
rm = struct('GroupNumber', {94,90, 92},...
    'Annotation', {'A', 'B','C'},...
    'Color', {'b', 'm','y'});
set(cgo,'RowLabels',countData.genes,'ColumnLabels',countData.sample(order))
%set(cgo,'ColumnLabels',countData.sample(order))%,'RowGroupMarker', rm)
plot(cgo)
%%
order_wt = get(cgo,'RowLabels');
order_wt = cellfun(@str2num,order_wt);
%% plot log2fold_2 same order as log2fold_wt
close all;
hm1 = HeatMap(countData.log2fold_1(:,order),...
    'DisplayRange',3,'Colormap',redbluecmap);
set(hm1,'ColumnLabels',countData.sample(order))

plot(hm1)
set(hm1,'RowLabels',countData.genes(:),...
    'ColumnLabels',countData.sample(order))
%
%save2pdf('log2Fold_2')


%%
for i =1:ncluster
    imagesc([countData.log2fold_1(idx==n,[7:10,2:5,12:15]);...
        countData.log2fold_1(idx==1,[7:10,2:5,12:15]);...
        countData.log2fold_1(idx==2,[7:10,2:5,12:15])],[-3,3])
end

%%
colorbar
%%
save2pdf('log2fold')
!open log2fold.pdf

%% plot
